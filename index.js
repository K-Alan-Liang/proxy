var http = require('http');
var net = require('net');
var url = require('url');
var ws=require("ws");

function request(cReq, cRes) {
    var u = url.parse(cReq.url);

    var options = {
        hostname : u.hostname, 
        port     : u.port || 80,
        path     : u.path,       
        method     : cReq.method,
        headers     : cReq.headers
    };

    var pReq = http.request(options, function(pRes) {
        cRes.writeHead(pRes.statusCode, pRes.headers);
        pRes.pipe(cRes);
    }).on('error', function(e) {
        cRes.end();
    });

    cReq.pipe(pReq);
    
}

function connect(cReq, cSock) {
    var u = url.parse('http://' + cReq.url);

    var pSock = net.connect(u.port, u.hostname, function() {
        cSock.write('HTTP/1.1 200 Connection Established\r\n\r\n');
        pSock.pipe(cSock);
    }).on('error', function(e) {
        cSock.end();
    });

    cSock.pipe(pSock);
}

var s=http.createServer();
    s.on('request', request)
    .on('connect', connect)
    .listen(8080, '0.0.0.0');


var wss=new ws.Server({server:s});
wss.on("connection",function(wsl,req){
	console.log("ws connection");
	var open,msg="";
	var c=net.createConnection(8080,'0.0.0.0',function(){
		console.log("conn created");
		open=true;
		if(msg)c.write(msg);
		c.on("data",function(d){try{wsl.send(d);console.log("data.",d.toString());}catch(e){}});
		c.on("close",function(){try{wsl.close(1000,"normal")}catch(e){}});
	});
	wsl.addEventListener("message",function(d){
		if(open)try{c.write(d.data);}catch(e){}
		else msg+=d.data;
		console.log("datacin",d.data.toString());
	//	console.dir(d);
	});
});